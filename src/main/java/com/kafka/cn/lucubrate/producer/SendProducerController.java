package com.kafka.cn.lucubrate.producer;


import com.alibaba.fastjson.JSONObject;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * 提供者发送消息: 1.主题；2.内容
 */

@Controller
@RequestMapping("/send-producer")
public class SendProducerController {

    @Resource
    private KafkaTemplate<Object, Object> kafkaTemplate;

    @RequestMapping("/send")
    @ResponseBody
    public String sendMessage(){
        Map<String, Object> map = new HashMap<>(3);
        map.put("id","1");
        map.put("name", "张三");
        map.put("age","20");
        kafkaTemplate.send("first", JSONObject.toJSONString(map));
        return "发送成功";
    }
}
