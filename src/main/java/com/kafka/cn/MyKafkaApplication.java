package com.kafka.cn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: yangShen
 * @Description:
 * @Date: 2020/3/31 10:48
 */
@SpringBootApplication
public class MyKafkaApplication {
    public static void main(String[] args) {
        SpringApplication.run(MyKafkaApplication.class,args);
    }
}
