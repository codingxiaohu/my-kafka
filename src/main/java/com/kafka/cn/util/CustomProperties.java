package com.kafka.cn.util;

import org.apache.kafka.clients.producer.ProducerConfig;

import java.util.Properties;

/**
 * @author: yangShen
 * @Description:
 * @Date: 2020/4/1 10:20
 */
public class CustomProperties {

    public static Properties getProducerProperties(){
        // 1. 创建kafka生产者的配置信息
        Properties properties = new Properties();
        // 2.指定kafka连接的集群，broker-list
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "10.16.26.16:9092");
        // 3.ACK应答级别
        properties.put(ProducerConfig.ACKS_CONFIG, "all");
        // 4.重试次数
        properties.put("retries", 3);
        // 5.批次大小：16k
        properties.put("batch.size", 16384);
        // 6.等待时间,超过1ms会自动断开连接
        properties.put("linger.ms", 1);
        // 7.RecordAccumulator 缓冲区大小：32M
        properties.put("buffer.memory", 33554432);
        // 8.key,value序列化类
        properties.put("key.serializer","org.apache.kafka.common.serialization.StringSerializer");
        properties.put("value.serializer","org.apache.kafka.common.serialization.StringSerializer");
        return properties;
    }

    /**
     * 消费者的资源配置
     * @return 消费者的资源配置
     */
    public static Properties getConsumerProperties(){
        Properties properties = new Properties();
        properties.put("","");
        return properties;
    }
}
