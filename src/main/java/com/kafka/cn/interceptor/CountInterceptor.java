package com.kafka.cn.interceptor;

import org.apache.kafka.clients.producer.ProducerInterceptor;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import java.util.Map;

/**
 * @author: yangShen
 * @Description:
 * @Date: 2020/4/1 10:07
 */
public class CountInterceptor implements ProducerInterceptor {

    private int success;
    private int error;

    @Override
    public void configure(Map<String, ?> configs) {

    }

    @Override
    public ProducerRecord onSend(ProducerRecord record) {
        return record;
    }

    @Override
    public void onAcknowledgement(RecordMetadata metadata, Exception exception) {
        if (null != metadata){
            success++;
        }else {
            error++;
        }
    }

    @Override
    public void close() {
        System.out.println("success : " + success);
        System.out.println("error : " + error);
    }


}
